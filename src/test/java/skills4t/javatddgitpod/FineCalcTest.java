
package com.skills4t.javatddgitpod;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FineCalcTest {

    @Test
    public void checkFine() {
       FineCalc fineCalc = new FineCalc(); 
       int res = fineCalc.checkFine(100, "city");
       assertEquals(100, res);
    }

    @Test
    public void checkCityFine() {
       FineCalc fineCalc = new FineCalc(); 
       int res = fineCalc.checkFine(75, "city");
       assertEquals(100, res);
    }

    @Test
    public void checkCountryFine() {
       FineCalc fineCalc = new FineCalc(); 
       int res = fineCalc.checkFine(110, "country");
       assertEquals(100, res)
    }

    @Test
    public void checkHighwayFine() {
       FineCalc fineCalc = new FineCalc(); 
       int res = fineCalc.checkFine(150, "highway");
       assertEquals(100, res);
    }


}

package com.skills4t.javatddgitpod;


public class FineCalc {

    public double getLimitCoeficient(int baseline, int speed) {
        return (speed - baseline) * 1.2;
    }

    // public int baselineFee = 100;

    public double checkFine(int speed, String area) {

        if (area == "city" && speed > 50) {
            return getLimitCoeficient (50, speed) * 120;
        }

        if (area == "country" && speed > 90) {
            return getLimitCoeficient (90, speed) * 110;
        }
        
        if (area == "highway" && speed > 130) {
            return getLimitCoeficient (130, speed) * 100;
        }
        
        return 0;

    }
}
